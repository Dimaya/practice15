"use strict";

// к сожалению лишней недели не хватило, не понимаю как сделать обрезку, явно что либо через 'slice' либо через 'substr'
// сортирровку не успеваю сделать. я думаю надо сделать функцию через 'sort'
// не знаю правильно ли будет действовать по принципу "лучше один раз вовремя, чем два раза правильно", но выбора у меня нет() 


const personalMovieDB = {
    actors: {},
    genres: [],
    privat: true, //*оставил "true" для теста //
    confirmForm: function(callback) {
        const confirm = document.body.querySelector('#confirm'),
            ul = document.body.querySelector('ul');

        confirm.addEventListener('click', (e) => {
            e.preventDefault();
            let film = document.body.querySelector('input').value;          

            
            personalMovieDB.movies = Object.assign(personalMovieDB.movies, film)
            
            ul.insertAdjacentHTML('afterbegin','<li></li>');
            
            let li = document.body.querySelector('li')
                li.insertAdjacentText('afterbegin',`${film}`)
                li.insertAdjacentHTML('beforeend','<input id="checkbox" type="checkbox" name="like" value="indeterminate " >Сделать любимым')
            let checkbox = document.body.querySelector('#checkbox')
            checkbox.addEventListener('change', (check) =>{ 
                if (checkbox.checked){ 
                    console.log('Добавляем любимый фильм')
                }
            })
                li.insertAdjacentHTML('beforeend','<button><img src="img/deleteIcon.png" alt="" id="del"></button>');
            let del = document.body.querySelector('#del');
            del.addEventListener('click', (ev) =>{
                li.remove()
            });        
                console.log(personalMovieDB.movies)

        });
    },
    numberOfFilms: function () {
        let askForCount = prompt('Сколько фильмов вы уже посмотрели?', '12');
        while (askForCount == null) {
            askForCount = prompt('Сколько фильмов вы уже посмотрели?', '');
        }
        while (askForCount == 0) {
            askForCount = prompt('Сколько фильмов вы уже посмотрели?', '');
        }
        const count = askForCount
        personalMovieDB.count = count
    },
    movie: function () {
        let askForMovies = prompt('Один из последних просмотренных фильмов?', 'logan');
        while (askForMovies == null) {
            askForMovies = prompt('Один из последних просмотренных фильмов?', '');
        }
        while (askForMovies.length >= 50) {
            askForMovies = prompt('Один из последних просмотренных фильмов?', '');
        };
        const rating = prompt('На сколько оцените его?', '8.1')
        const movies = {
            [askForMovies]: (`${rating}`)
        };
        personalMovieDB.movies = movies;
    },
    counter: function (callback) {
        if (personalMovieDB.count <= 10) {
            alert('Просмотрено довольно мало фильмов');
        } else if (personalMovieDB.count <= 30) {
            alert('Вы классический зритель');
        } else if (personalMovieDB.count > 30) {
            alert('Вы киноман');
        } else {
            alert('Произошла ошибка');
        }
    },
    writeYourGenres: function () {
        personalMovieDB.genres[0] = prompt('Ваш любимый жанр под номером 1?', 'триллер');
        while (personalMovieDB.genres[0] == null) {
            personalMovieDB.genres[0] = prompt('Ваш любимый жанр под номером 1?', 'триллер');
        };
        while (personalMovieDB.genres[0] == 0) {
            personalMovieDB.genres[0] = prompt('Ваш любимый жанр под номером 1?', 'триллер');
        }
        personalMovieDB.genres[1] = prompt('Ваш любимый жанр под номером 2?', 'ужасы');
        while (personalMovieDB.genres[1] == null) {
            personalMovieDB.genres[1] = prompt('Ваш любимый жанр под номером 2?', 'ужасы');
        };
        while (personalMovieDB.genres[1] == 0) {
            personalMovieDB.genres[1] = prompt('Ваш любимый жанр под номером 2?', 'ужасы');
        }
        personalMovieDB.genres[2] = prompt('Ваш любимый жанр под номером 3?', 'комедия');
        while (personalMovieDB.genres[2] == null) {
            personalMovieDB.genres[2] = prompt('Ваш любимый жанр под номером 3?', 'комедия');
        };
        while (personalMovieDB.genres[2] == 0) {
            personalMovieDB.genres[2] = prompt('Ваш любимый жанр под номером 3?', 'комедия');
        }
    },
    likeGenres: function (callback) {
        personalMovieDB.genres.forEach(function (item, i, [genres]) {
            console.log(`Любимый жанр ${i} это ${item}`)
        })
    },
    showMyDB: function () {
        // надо ли тут создавать 'callback' функцию от 'toggeVisibleMyDB'? в данном контексте я решил не задавать, 
        //но думаю если бы переменную 'privat' мы бы меняли в задании то тогда я бы делал уже через callback. //
        if (personalMovieDB.privat == false) {
            console.log(personalMovieDB);
        };
    },
    toggleVisibleMyDB: function () {
        if (personalMovieDB.privat == false) {
            personalMovieDB.privat = true;
        } else if (personalMovieDB.privat == true) {
            personalMovieDB.privat = false
        };
    },

};
personalMovieDB.numberOfFilms();
personalMovieDB.movie();
personalMovieDB.confirmForm(personalMovieDB.movie)
personalMovieDB.counter(personalMovieDB.numberOfFilms);
personalMovieDB.writeYourGenres();
personalMovieDB.likeGenres(personalMovieDB.writeYourGenres);
personalMovieDB.toggleVisibleMyDB();
personalMovieDB.showMyDB();

// /////////////////////////////////////////////////////////////////////////
// console.log(document.head)
// console.log(document.documentElement)
// console.log(document.body.childNodes)
// console.log(document.body.firstChild)
// console.log(document.body.firstElementChild)
// console.log(document.body.lastChild)
// console.log(document.querySelector('#current').parentNode.parentNode)
// console.log(document.querySelector('#current').parentElement)
// console.log(document.querySelector('[data-current= "3"]').nextElementSibling)

// for(let node of document.body.childNodes){
//     if(node.nodeName == '#text'){
//         continue
//     }

//     console.log(node)
// }

////////////////////////////////////////////////////////////////////
// const btns = document.querySelectorAll('button'),
//     overlay = document.querySelector('.overlay');

// let i = 0;
// const deleteElement = (e) => {
//     console.log(e.currentTarget)
//     console.log(e.type)

//     i++
//     if (i == 1){
//         btn.removeEventListener('click', deleteElement); 

//     };
// };

// btn.addEventListener('click', deleteElement); 
// overlay.addEventListener('click', deleteElement); 

// btns.forEach(btn => {
//         btn.addEventListener('click', deleteElement, {once: true})
// })

// const link = document.querySelector('a');

// link.addEventListener('click', (event) => {
//     event.preventDefault();

//     console.log(event.target)
// })
//////////////////////////////////////////////////////////////////////

